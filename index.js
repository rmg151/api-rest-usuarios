
'use strict' 

const port = process.env.PORT || 6000;
//const URL_DB = "localhost:27017/SD-01"

const URL_DB = "mongodb+srv://rmg151:dieciocho@cluster0.6mqfl.mongodb.net/usuarios?retryWrites=true&w=majority";


//incorporar librerias:
const https = require('https');
const logger = require('morgan');
const express = require('express');
const mongojs = require('mongojs');
const moment = require('moment');
const fs = require('fs');
//const { Certificate } = require('crypto');
const passService = require('./services/pass.service');
const tokenService = require('./services/token.service');

const opciones = {
    key: fs.readFileSync('./cert/key.pem'),
    cert: fs.readFileSync('./cert/cert.pem')
}; 


//declaramos nuestra aplicacion de tipo express:
const app = express(); 

//variable de mi base de datos:
var db = mongojs(URL_DB); 
var id = mongojs.ObjectID; //funcion para convertir un id textual en un objeto de mongojs

//declaramos middleware:
app.use(logger('dev'));
app.use(express.urlencoded({extended: false})); //para leer objetos de formularios y poder entender el body de las diferentes r4equest
app.use(express.json()); //para reconocer objetos json

//middleware que cada vez que cambie lo que viene en colecciones, llama a la funcion calllback
app.param("colecciones", (request, response, next, coleccion) => {
    console.log('param /api/:colecciones');
    console.log('coleccion: ', coleccion);

    request.collection = db.collection(coleccion); //creamos puntero a una funcion que apunta a la bd y la tabla indicadas
    return next(); //invocar al siguiente middleware; puntero a la siguiente funcion, o middleware
});


//Autorizacion:
function auth(request, response, next) {
    if (! request.headers.authorization) {
        response.status(401).json({
            result:'KO',
            mensajes: "no has enviado el token en la cabecera"
        })
        return next();
    } 
    
    console.log(request.headers.authorization);

    if( request.headers.authorization.split(" ")[1] == "MITOKEN123456789"){ //token en formato JWT
        return next();
    }

    response.status(401).json({
        result:'KO',
        mensajes: "no has enviado el token en la cabecera"
    })

    return next(new Error("Acceso no autorizado a este servicio. ")); 
}



//Routes y Controllers:

//Obtener todas las colecciones de coches que hay
app.get('/api/', (request, response, next) => {
    console.log('GET /api');
    console.log(request.params); //estan todos los parametros que se pasan
    console.log(request.collection); //devuelve la coleccion a la que apunte

    //cuando alguien llame a /api, me imprimirá todas las colecciones que hay:
    db.getCollectionNames((err, colecciones) => {
        if(err) return next(err); //si hay error llamo a la siguiente funcion propagando el error
        console.log(colecciones);
        response.json({result: 'OK', colecciones: colecciones}); //sino devuelvo las colecciones
    });

});


//obtener todos los objetos de una tabla
app.get('/api/:colecciones', (request, response, next) => {
    console.log('GET /api:colecciones');
    console.log(request.params); 
    console.log(request.collection); 

    request.collection.find((err, coleccion) => {
        if(err) return next(err); //propagamos el error
        console.log(coleccion);
        response.json({
            result: 'OK', 
            coleccion: request.params.colecciones, 
            elementos: coleccion
        });
    
    }); //busca todo lo que hay en esa coleccion

});


//Obtener un producto en concreto por el id:
app.get('/api/:colecciones/:id', (request, response, next) => {
    request.collection.findOne({_id: id(request.params.id)}, (err, coleccion) => {
    if (err) return next(err);
    response.json(coleccion);
   });
});

/*
//crear un elemento:
app.post('/api/:colecciones', auth, (request, response, next) => {
    const nuevoElemento = request.body; 

    request.collection.findOne({correo: (request.body.correo)}, (err, coleccion) => {

        if(coleccion) {
            response.status(401).json({
                result: 'Usuario ya registrado'
            });} 
        else {    
             request.collection.save(nuevoElemento, (err, elementoGuardado) => {
                        if(err) return next(err);

                        response.status(201).json({
                            result: 'OK',
                            coleccion: request.params.colecciones,
                            elemento: elementoGuardado
                        });
                    }); 
        }
    });
});
*/

app.post('/api/:colecciones/registrar', (request, response, next) => {
    const nuevoElemento = request.body; 
    const usu = {
        correo: nuevoElemento.correo,
        nombre: nuevoElemento.nombre,
        password: nuevoElemento.password
    }

    request.collection.findOne({correo: (request.body.correo)}, (err, coleccion) => {

        if(coleccion) {
            response.status(401).json({
                result: 'Usuario ya registrado'
            });
        } else {    

            passService.encriptaPassword(usu.password)
            .then(hash => {
                usu.password = hash;

                request.collection.save(nuevoElemento, (err, elementoGuardado) => {
                        if(err) return next(err);

                        response.status(201).json({
                            result: 'OK',
                            coleccion: request.params.colecciones,
                            elemento: elementoGuardado,
                            hash: hash
                        });
                    }); 
                })
        }
    });
});


app.post('/api/:colecciones/iniciarsesion', (request, response, next) => {
    const nuevoElemento = request.body; 
    
    request.collection.findOne({correo: (request.body.correo)}, (err, coleccion) => {

        if(!coleccion) {
            response.status(401).json({
                result: 'Usuario no registrado'
            });
        } else {    

            const token = tokenService.creaToken(coleccion);
            response.status(201).json({
                result: 'OK',
                token: token
            });
        }
    });
});


app.put('/api/:colecciones/:id', (request, response, next) => {
    const elementoId = request.params.id;
    const queColeccion = request.params.colecciones;
    const elementoNuevo = request.body;
    request.collection.update(
        {_id: id(elementoId)}, //convierte cadena de texto a identificador
        {$set: elementoNuevo}, //añadir el elemento nuevo
        {safe: true, multi: false}, (err, elementoModif) => {
            if (err) return next(err);
            console.log(elementoModif);
            response.json(elementoModif);
        });
}); 


app.delete('/api/:colecciones/:id', auth, (request, response, next) => {
    let elementoId = request.params.id;
    
    request.collection.remove({_id: id(elementoId)}, (err, resultado) => {
    if (err) return next(err);
    response.json(resultado);
    });
});


https.createServer( opciones, app ).listen(port, () => {
    console.log(`API RESTFul CRUD ejecutandose en https://localhost:${port}/api/{colecciones}/{id}`);
});

/**

app.listen(port, () => {
    console.log(`API RESTFul CRUD ejecutandose en http://localhost:${port}/api/{colecciones}/{id}`);
});


//Encriptamos pass
passService.encriptaPassword(usu.password)

    .then( hash => {
        usu.password = hash;
    })

*/
